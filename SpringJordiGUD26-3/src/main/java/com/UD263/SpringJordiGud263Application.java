package com.UD263;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringJordiGud263Application {

	public static void main(String[] args) {
		SpringApplication.run(SpringJordiGud263Application.class, args);
	}

}
