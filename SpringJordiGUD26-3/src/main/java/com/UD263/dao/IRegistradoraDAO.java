package com.UD263.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.UD263.dto.Registradora;

public interface IRegistradoraDAO extends JpaRepository<Registradora, Integer>{

}
