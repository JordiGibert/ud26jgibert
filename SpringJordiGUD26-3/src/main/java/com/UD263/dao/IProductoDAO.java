package com.UD263.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.UD263.dto.Producto;

public interface IProductoDAO extends JpaRepository<Producto, Integer>{

}
