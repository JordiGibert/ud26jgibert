package com.UD263.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.UD263.dto.Venta;

public interface IVentaDAO extends JpaRepository<Venta, Integer>{

}
