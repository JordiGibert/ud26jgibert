package com.UD263.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.UD263.dto.Cajero;


public interface ICajeroDAO extends JpaRepository<Cajero, Integer>{

}
