package com.UD263.dto;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="venta")
public class Venta {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	
	@ManyToOne
    @JoinColumn(name = "cajero")
	Cajero cajero;
	
	@ManyToOne
    @JoinColumn(name = "producto")
	Producto producto;
	
	@ManyToOne
    @JoinColumn(name = "maquina")
	Registradora registradora;
	
	public Venta() {
		
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Cajero getCajero() {
		return cajero;
	}

	public void setCajero(Cajero cajero) {
		this.cajero = cajero;
	}

	public Producto getProducto() {
		return producto;
	}

	public void setProducto(Producto producto) {
		this.producto = producto;
	}

	public Registradora getRegistradora() {
		return registradora;
	}

	public void setRegistradora(Registradora registradora) {
		this.registradora = registradora;
	}
	
	
	
}
