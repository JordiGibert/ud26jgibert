package com.UD263.service;

import java.util.List;

import com.UD263.dto.Registradora;

public interface IRegistradoraService {
	public List<Registradora> listRegistradora(); 
	
	public Registradora saveRegistradora(Registradora registradora);	
	
	public Registradora registradoraXID(Integer id); 
	
	public Registradora updateRegistradora(Registradora registradora); 
	
	public void deleteRegistradora(Integer id);
	
}
