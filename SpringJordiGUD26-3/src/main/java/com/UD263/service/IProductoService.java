package com.UD263.service;

import java.util.List;

import com.UD263.dto.Producto;

public interface IProductoService {

	public List<Producto> listProducto(); 
	
	public Producto saveProducto(Producto producto);	
	
	public Producto productoXID(Integer id); 
	
	public Producto updateProducto(Producto producto); 
	
	public void deleteProducto(Integer id);
}
