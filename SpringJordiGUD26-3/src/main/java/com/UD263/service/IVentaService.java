package com.UD263.service;

import java.util.List;

import com.UD263.dto.Venta;

public interface IVentaService {

	public List<Venta> listVenta(); 
	
	public Venta saveVenta(Venta venta);	
	
	public Venta ventaXID(Integer id); 
	
	public Venta updateVenta(Venta venta); 
	
	public void deleteVenta(Integer id);
}
