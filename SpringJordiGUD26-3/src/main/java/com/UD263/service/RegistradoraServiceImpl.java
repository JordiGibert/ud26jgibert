package com.UD263.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.UD263.dao.IRegistradoraDAO;
import com.UD263.dto.Registradora;

@Service
public class RegistradoraServiceImpl implements IRegistradoraService{

	@Autowired
	IRegistradoraDAO iRegistradoraDAO;
	
	@Override
	public List<Registradora> listRegistradora() {
		// TODO Auto-generated method stub
		return iRegistradoraDAO.findAll();
	}

	@Override
	public Registradora saveRegistradora(Registradora registradora) {
		// TODO Auto-generated method stub
		return iRegistradoraDAO.save(registradora);
	}

	@Override
	public Registradora registradoraXID(Integer id) {
		// TODO Auto-generated method stub
		return iRegistradoraDAO.findById(id).get();
	}

	@Override
	public Registradora updateRegistradora(Registradora registradora) {
		// TODO Auto-generated method stub
		return iRegistradoraDAO.save(registradora);
	}

	@Override
	public void deleteRegistradora(Integer id) {
		// TODO Auto-generated method stub
		iRegistradoraDAO.deleteById(id);
	}

}
