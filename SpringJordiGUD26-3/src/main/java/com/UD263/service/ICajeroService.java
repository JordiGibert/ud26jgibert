package com.UD263.service;

import java.util.List;

import com.UD263.dto.Cajero;



public interface ICajeroService {
	public List<Cajero> listCajero(); 
	
	public Cajero saveCajero(Cajero cajero);	
	
	public Cajero cajeroXID(Integer id); 
	
	public Cajero updateCajero(Cajero cajero); 
	
	public void deleteCajero(Integer id);
}
