package com.UD263.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.UD263.dto.Venta;

import com.UD263.service.VentaServiceImpl;

@RestController
@RequestMapping("/api")
public class VentaController {
	@Autowired
	VentaServiceImpl ventaServiceImpl;
	
	@GetMapping("/ventas")
	public List<Venta> listarVenta(){
		return ventaServiceImpl.listVenta();
	}
	
	
	@PostMapping("/ventas")
	public Venta salvarVenta(@RequestBody Venta venta) {
		
		return ventaServiceImpl.saveVenta(venta);
	}
	
	
	@GetMapping("/ventas/{id}")
	public Venta VentaXID(@PathVariable(name="id") Integer id) {
		
		Venta venta_xid= new Venta();
		
		venta_xid=ventaServiceImpl.ventaXID(id);
		
		System.out.println("Venta XID: "+venta_xid);
		
		return venta_xid;
	}
	
	@PutMapping("/ventas/{id}")
	public Venta actualizarVenta(@PathVariable(name="id")Integer id,@RequestBody Venta venta) {
		
		Venta venta_seleccionado= new Venta();
		Venta venta_actualizado= new Venta();
		
		venta_seleccionado= ventaServiceImpl.ventaXID(id);
		
		venta_seleccionado.setProducto(venta.getProducto());
		venta_seleccionado.setCajero(venta.getCajero());
		venta_seleccionado.setRegistradora(venta.getRegistradora());
		
		
		venta_actualizado = ventaServiceImpl.updateVenta(venta_seleccionado);
		
		System.out.println("Las ventas actualizadas son: "+ venta_actualizado);
		
		return venta_actualizado;
	}
	
	@DeleteMapping("/ventas/{id}")
	public void eleiminarVenta(@PathVariable(name="id")Integer id) {
		ventaServiceImpl.deleteVenta(id);
	}

}
