package com.UD263.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


import com.UD263.dto.Registradora;
import com.UD263.service.RegistradoraServiceImpl;

@RestController
@RequestMapping("/api")
public class RegistradoraController {
	@Autowired
	RegistradoraServiceImpl registradoraServiceImpl; 
	
	@GetMapping("/registradoras")
	public List<Registradora> listarRegistradora(){
		return registradoraServiceImpl.listRegistradora();
	}
	
	
	@PostMapping("/registradoras")
	public Registradora salvarRegistradora(@RequestBody Registradora registradora) {
		
		return registradoraServiceImpl.saveRegistradora(registradora);
	}
	
	
	@GetMapping("/registradoras/{id}")
	public Registradora RegistradoraXID(@PathVariable(name="id") Integer id) {
		
		Registradora Registradora_xid= new Registradora();
		
		Registradora_xid=registradoraServiceImpl.registradoraXID(id);
		
		System.out.println("Registradora XID: "+Registradora_xid);
		
		return Registradora_xid;
	}
	
	@PutMapping("/registradoras/{id}")
	public Registradora actualizarRegistradora(@PathVariable(name="id")Integer id,@RequestBody Registradora registradora) {
		
		Registradora registradora_seleccionado= new Registradora();
		Registradora registradora_actualizado= new Registradora();
		
		registradora_seleccionado= registradoraServiceImpl.registradoraXID(id);
		
		registradora_seleccionado.setPiso(registradora.getPiso());
		
		registradora_actualizado = registradoraServiceImpl.updateRegistradora(registradora_seleccionado);
		
		System.out.println("El Cientifico actualizado es: "+ registradora_actualizado);
		
		return registradora_actualizado;
	}
	
	@DeleteMapping("/registradoras/{id}")
	public void eleiminarRegistradora(@PathVariable(name="id")Integer id) {
		registradoraServiceImpl.deleteRegistradora(id);
	}
}
