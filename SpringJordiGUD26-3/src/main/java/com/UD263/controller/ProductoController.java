package com.UD263.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.UD263.dto.Producto;
import com.UD263.service.ProductoServiceImpl;



@RestController
@RequestMapping("/api")
public class ProductoController {
	@Autowired
	ProductoServiceImpl productoServiceImpl;
	
	@GetMapping("/productos")
	public List<Producto> listarProducto(){
		return productoServiceImpl.listProducto();
	}
	
	
	@PostMapping("/productos")
	public Producto salvarProducto(@RequestBody Producto producto) {
		
		return productoServiceImpl.saveProducto(producto);
	}
	
	
	@GetMapping("/productos/{id}")
	public Producto ProductoXID(@PathVariable(name="id") Integer id) {
		
		Producto producto_xid= new Producto();
		
		producto_xid=productoServiceImpl.productoXID(id);
		
		System.out.println("Registradora XID: "+producto_xid);
		
		return producto_xid;
	}
	
	@PutMapping("/productos/{id}")
	public Producto actualizarProducto(@PathVariable(name="id")Integer id,@RequestBody Producto producto) {
		
		Producto producto_seleccionado= new Producto();
		Producto producto_actualizado= new Producto();
		
		producto_seleccionado= productoServiceImpl.productoXID(id);
		
		producto_seleccionado.setNombre(producto.getNombre());
		producto_seleccionado.setPrecio(producto.getPrecio());
		
		
		producto_actualizado = productoServiceImpl.updateProducto(producto_seleccionado);
		
		System.out.println("El Producto actualizado es: "+ producto_actualizado);
		
		return producto_actualizado;
	}
	
	@DeleteMapping("/productos/{id}")
	public void eleiminarProducto(@PathVariable(name="id")Integer id) {
		productoServiceImpl.deleteProducto(id);
	}
}
