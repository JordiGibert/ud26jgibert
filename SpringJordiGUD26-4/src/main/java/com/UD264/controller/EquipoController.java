package com.UD264.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.UD264.dto.Equipo;
import com.UD264.service.EquipoServiceImpl;

@RestController
@RequestMapping("/api")
public class EquipoController {
	@Autowired
	EquipoServiceImpl equipoServiceImpl; 
	
	@GetMapping("/equipos")
	public List<Equipo> listarCajero(){
		return equipoServiceImpl.listEquipo();
	}
	
	
	@PostMapping("/equipos")
	public Equipo salvarEquipo(@RequestBody Equipo equipo) {
		
		return equipoServiceImpl.saveEquipo(equipo);
	}
	
	
	@GetMapping("/equipos/{id}")
	public Equipo equipoXID(@PathVariable(name="id") Integer id) {
		
		Equipo facultad_xid= new Equipo();
		
		facultad_xid=equipoServiceImpl.equipoXID(id);
		
		System.out.println("Equipo XID: "+facultad_xid);
		
		return facultad_xid;
	}
	
	@PutMapping("/equipos/{id}")
	public Equipo actualizarEquipo(@PathVariable(name="id")Integer id,@RequestBody Equipo equipo) {
		
		Equipo equipo_seleccionado= new Equipo();
		Equipo equipo_actualizado= new Equipo();
		
		equipo_seleccionado= equipoServiceImpl.equipoXID(id);
		
		equipo_seleccionado.setNombre(equipo.getNombre());
		
		equipo_seleccionado.setSerie(equipo.getSerie());
		
		equipo_seleccionado.setFacultad(equipo.getFacultad());
		
		equipo_actualizado = equipoServiceImpl.updateEquipo(equipo_seleccionado);
		
		System.out.println("El equipo actualizado es: "+ equipo_actualizado);
		
		return equipo_actualizado;
	}
	
	@DeleteMapping("/equipos/{id}")
	public void eliminarEquipo(@PathVariable(name="id")Integer id) {
		equipoServiceImpl.deleteEquipo(id);
	}
}
