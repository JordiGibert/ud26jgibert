package com.UD264.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.UD264.dto.Facultad;
import com.UD264.service.FacultadServiceImpl;



@RestController
@RequestMapping("/api")
public class FacultadController {
	@Autowired
	FacultadServiceImpl facultadServiceImpl; 
	
	@GetMapping("/facultades")
	public List<Facultad> listarCajero(){
		return facultadServiceImpl.listFacultad();
	}
	
	
	@PostMapping("/facultades")
	public Facultad salvarFacultad(@RequestBody Facultad facultad) {
		
		return facultadServiceImpl.saveFacultad(facultad);
	}
	
	
	@GetMapping("/facultades/{id}")
	public Facultad facultadXID(@PathVariable(name="id") Integer id) {
		
		Facultad facultad_xid= new Facultad();
		
		facultad_xid=facultadServiceImpl.facultadXID(id);
		
		System.out.println("Facultad XID: "+facultad_xid);
		
		return facultad_xid;
	}
	
	@PutMapping("/facultades/{id}")
	public Facultad actualizarFacultad(@PathVariable(name="id")Integer id,@RequestBody Facultad cajero) {
		
		Facultad facultad_seleccionado= new Facultad();
		Facultad facultad_actualizado= new Facultad();
		
		facultad_seleccionado= facultadServiceImpl.facultadXID(id);
		
		facultad_seleccionado.setNombre(cajero.getNombre());
		
		facultad_actualizado = facultadServiceImpl.updateFacultad(facultad_seleccionado);
		
		System.out.println("La facultad actualizada es: "+ facultad_actualizado);
		
		return facultad_actualizado;
	}
	
	@DeleteMapping("/facultades/{id}")
	public void eleiminarFacultad(@PathVariable(name="id")Integer id) {
		facultadServiceImpl.deleteFacultad(id);
	}
}
