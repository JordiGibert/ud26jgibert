package com.UD264.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.UD264.dto.Investigador;
import com.UD264.service.InvestigadorServiceImpl;

@RestController
@RequestMapping("/api")
public class InvestigadorController {

	@Autowired
	InvestigadorServiceImpl investigadorServiceImpl; 
	
	@GetMapping("/investigadores")
	public List<Investigador> listarCajero(){
		return investigadorServiceImpl.listInvestigador();
	}
	
	
	@PostMapping("/investigadores")
	public Investigador salvarInvestigador(@RequestBody Investigador investigador) {
		
		return investigadorServiceImpl.saveInvestigador(investigador);
	}
	
	
	@GetMapping("/investigadores/{id}")
	public Investigador investigadorXID(@PathVariable(name="id") Integer id) {
		
		Investigador investigador_xid= new Investigador();
		
		investigador_xid=investigadorServiceImpl.investigadorXID(id);
		
		System.out.println("Investigador XID: "+investigador_xid);
		
		return investigador_xid;
	}
	
	@PutMapping("/investigadores/{id}")
	public Investigador actualizarInvestigador(@PathVariable(name="id")Integer id,@RequestBody Investigador investigador) {
		
		Investigador investigador_seleccionado= new Investigador();
		Investigador investigador_actualizado= new Investigador();
		
		investigador_seleccionado= investigadorServiceImpl.investigadorXID(id);
		
		investigador_seleccionado.setNombre(investigador.getNombre());
		
		investigador_seleccionado.setDNI(investigador.getDNI());
		
		investigador_seleccionado.setFacultad(investigador.getFacultad());
		
		investigador_actualizado = investigadorServiceImpl.updateInvestigador(investigador_seleccionado);
		
		System.out.println("El investigador actualizado es: "+ investigador_actualizado);
		
		return investigador_actualizado;
	}
	
	@DeleteMapping("/investigadores/{id}")
	public void eliminarInvestigador(@PathVariable(name="id")Integer id) {
		investigadorServiceImpl.deleteInvestigador(id);
	}
}
