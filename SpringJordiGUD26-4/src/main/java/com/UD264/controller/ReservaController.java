package com.UD264.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.UD264.dto.Reserva;
import com.UD264.service.ReservaServiceImpl;

@RestController
@RequestMapping("/api")
public class ReservaController {
	@Autowired
	ReservaServiceImpl reservaServiceImpl; 
	
	@GetMapping("/reservas")
	public List<Reserva> listarReserva(){
		return reservaServiceImpl.listReserva();
	}
	
	
	@PostMapping("/reservas")
	public Reserva salvarReserva(@RequestBody Reserva reserva) {
		
		return reservaServiceImpl.saveReserva(reserva);
	}
	
	
	@GetMapping("/reservas/{id}")
	public Reserva reservaXID(@PathVariable(name="id") Integer id) {
		
		Reserva reserva_xid= new Reserva();
		
		reserva_xid=reservaServiceImpl.reservaXID(id);
		
		System.out.println("Reserva XID: "+reserva_xid);
		
		return reserva_xid;
	}
	
	@PutMapping("/reservas/{id}")
	public Reserva actualizarReserva(@PathVariable(name="id")Integer id,@RequestBody Reserva reserva) {
		
		Reserva reserva_seleccionado= new Reserva();
		Reserva reserva_actualizado= new Reserva();
		
		reserva_seleccionado= reservaServiceImpl.reservaXID(id);
		
		reserva_seleccionado.setInvestigador(reserva.getInvestigador());
		
		reserva_seleccionado.setEquipo(reserva.getEquipo());
		
		reserva_seleccionado.setComienzo(reserva.getComienzo());
		
		reserva_seleccionado.setFin(reserva.getFin());
		
		reserva_actualizado = reservaServiceImpl.updateReserva(reserva_seleccionado);
		
		System.out.println("La reserva actualizada es: "+ reserva_actualizado);
		
		return reserva_actualizado;
	}
	
	@DeleteMapping("/reservas/{id}")
	public void eliminarReserva(@PathVariable(name="id")Integer id) {
		reservaServiceImpl.deleteReserva(id);
	}
}
