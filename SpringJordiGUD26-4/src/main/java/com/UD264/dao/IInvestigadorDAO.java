package com.UD264.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.UD264.dto.Investigador;

public interface IInvestigadorDAO extends JpaRepository<Investigador, Integer>{

}
