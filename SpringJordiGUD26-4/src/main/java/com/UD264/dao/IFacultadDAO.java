package com.UD264.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.UD264.dto.Facultad;
public interface IFacultadDAO extends JpaRepository<Facultad, Integer> {

}
