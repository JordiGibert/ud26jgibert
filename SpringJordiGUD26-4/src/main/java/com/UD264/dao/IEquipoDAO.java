package com.UD264.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.UD264.dto.Equipo;

public interface IEquipoDAO extends JpaRepository<Equipo, Integer> {

}
