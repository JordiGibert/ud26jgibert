package com.UD264.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.UD264.dto.Reserva;

public interface IReservaDAO extends JpaRepository<Reserva, Integer> {

}
