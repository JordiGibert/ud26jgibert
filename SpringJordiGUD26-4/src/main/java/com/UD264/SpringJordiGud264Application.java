package com.UD264;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringJordiGud264Application {

	public static void main(String[] args) {
		SpringApplication.run(SpringJordiGud264Application.class, args);
	}

}
