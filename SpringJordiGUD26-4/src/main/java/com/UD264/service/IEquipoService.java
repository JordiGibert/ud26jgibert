package com.UD264.service;

import java.util.List;

import com.UD264.dto.Equipo;


public interface IEquipoService {

	public List<Equipo> listEquipo(); 
	
	public Equipo saveEquipo(Equipo equipo);	
	
	public Equipo equipoXID(Integer id); 
	
	public Equipo updateEquipo(Equipo equipo); 
	
	public void deleteEquipo(Integer id);
}
