package com.UD264.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.UD264.dao.IReservaDAO;
import com.UD264.dto.Reserva;

@Service
public class ReservaServiceImpl implements IReservaService{
	@Autowired
	IReservaDAO iReservaDAO;

	@Override
	public List<Reserva> listReserva() {
		// TODO Auto-generated method stub
		return iReservaDAO.findAll();
	}

	@Override
	public Reserva saveReserva(Reserva reserva) {
		// TODO Auto-generated method stub
		return iReservaDAO.save(reserva);
	}

	@Override
	public Reserva reservaXID(Integer id) {
		// TODO Auto-generated method stub
		return iReservaDAO.findById(id).get();
	}

	@Override
	public Reserva updateReserva(Reserva reserva) {
		// TODO Auto-generated method stub
		return iReservaDAO.save(reserva);
	}

	@Override
	public void deleteReserva(Integer id) {
		// TODO Auto-generated method stub
		iReservaDAO.deleteById(id);
	}
}
