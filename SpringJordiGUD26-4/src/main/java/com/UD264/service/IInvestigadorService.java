package com.UD264.service;

import java.util.List;

import com.UD264.dto.Investigador;

public interface IInvestigadorService {
	public List<Investigador> listInvestigador(); 
	
	public Investigador saveInvestigador(Investigador investigador);	
	
	public Investigador investigadorXID(Integer id); 
	
	public Investigador updateInvestigador(Investigador investigador); 
	
	public void deleteInvestigador(Integer id);
}
