package com.UD264.service;

import java.util.List;

import com.UD264.dto.Reserva;

public interface IReservaService {
	public List<Reserva> listReserva(); 
	
	public Reserva saveReserva(Reserva reserva);	
	
	public Reserva reservaXID(Integer id); 
	
	public Reserva updateReserva(Reserva reserva); 
	
	public void deleteReserva(Integer id);
}
