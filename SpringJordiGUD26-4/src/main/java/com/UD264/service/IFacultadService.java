package com.UD264.service;

import java.util.List;

import com.UD264.dto.Facultad;

public interface IFacultadService {
	public List<Facultad> listFacultad(); 
	
	public Facultad saveFacultad(Facultad facultad);	
	
	public Facultad facultadXID(Integer id); 
	
	public Facultad updateFacultad(Facultad facultad); 
	
	public void deleteFacultad(Integer id);
}
