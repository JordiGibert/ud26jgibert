package com.UD264.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.UD264.dao.IEquipoDAO;
import com.UD264.dto.Equipo;

@Service
public class EquipoServiceImpl implements IEquipoService {

	@Autowired
	IEquipoDAO IEquipoDAO;
	
	@Override
	public List<Equipo> listEquipo() {
		// TODO Auto-generated method stub
		return IEquipoDAO.findAll();
	}

	@Override
	public Equipo saveEquipo(Equipo equipo) {
		// TODO Auto-generated method stub
		return IEquipoDAO.save(equipo);
	}

	@Override
	public Equipo equipoXID(Integer id) {
		// TODO Auto-generated method stub
		return IEquipoDAO.findById(id).get();
	}

	@Override
	public Equipo updateEquipo(Equipo equipo) {
		// TODO Auto-generated method stub
		return IEquipoDAO.save(equipo);
	}

	@Override
	public void deleteEquipo(Integer id) {
		// TODO Auto-generated method stub
		IEquipoDAO.deleteById(id);
	}

}
