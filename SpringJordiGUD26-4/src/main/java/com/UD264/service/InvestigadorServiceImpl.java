package com.UD264.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.UD264.dao.IInvestigadorDAO;
import com.UD264.dto.Investigador;

@Service
public class InvestigadorServiceImpl implements IInvestigadorService{

	@Autowired
	IInvestigadorDAO iInvestigadorDAO;

	@Override
	public List<Investigador> listInvestigador() {
		// TODO Auto-generated method stub
		return iInvestigadorDAO.findAll();
	}

	@Override
	public Investigador saveInvestigador(Investigador investigador) {
		// TODO Auto-generated method stub
		return iInvestigadorDAO.save(investigador);
	}

	@Override
	public Investigador investigadorXID(Integer id) {
		// TODO Auto-generated method stub
		return iInvestigadorDAO.findById(id).get();
	}

	@Override
	public Investigador updateInvestigador(Investigador investigador) {
		// TODO Auto-generated method stub
		return iInvestigadorDAO.save(investigador);
	}

	@Override
	public void deleteInvestigador(Integer id) {
		// TODO Auto-generated method stub
		iInvestigadorDAO.findById(id);
	}
}
