package com.UD261.dto;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name="cientificos")
public class Cientificos {
	@Id
	@Column(name = "DNI", length=8)
	private String DNI;
	@Column(name = "nom_apels")
	private String nom_apels;
	
	@OneToMany
    @JoinColumn(name="cientifico")
	private List<Asignado_a> asignado_a;
	
	public Cientificos() {
		
	}

	public Cientificos(String DNI, String nom_apels, List<Asignado_a> asignado_a) {
		super();
		this.DNI = DNI;
		this.nom_apels = nom_apels;
		this.asignado_a = asignado_a;
	}

	public String getDNI() {
		return DNI;
	}

	public void setDNI(String DNI) {
		this.DNI = DNI;
	}

	public String getNom_apels() {
		return nom_apels;
	}

	public void setNom_apels(String nom_apels) {
		this.nom_apels = nom_apels;
	}

	@JsonIgnore
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "Asignado_a")
	public List<Asignado_a> getAsignado_a() {
		return asignado_a;
	}

	public void setAsignado_a(List<Asignado_a> asignado_a) {
		this.asignado_a = asignado_a;
	}
/*
	@Override
	public String toString() {
		return "Cientificos [DNI=" + DNI + ", nom_apels=" + nom_apels + ", asignado_a=" + asignado_a + "]";
	}
	*/
	

}
