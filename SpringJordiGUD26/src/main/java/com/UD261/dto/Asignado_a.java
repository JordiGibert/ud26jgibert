package com.UD261.dto;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;



@Entity
@Table(name="asignado_a")
public class Asignado_a {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)//busca ultimo valor e incrementa desde id final de db
	private int id;
	
	@ManyToOne
    @JoinColumn(name = "cientifico")
    Cientificos cientificos;
 
    @ManyToOne
    @JoinColumn(name = "proyecto")
    Proyecto proyecto;
    
    public Asignado_a() {
    	
    }

	public Asignado_a(int id, Cientificos cientificos, Proyecto proyecto) {
		this.id = id;
		this.cientificos = cientificos;
		this.proyecto = proyecto;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Cientificos getCientificos() {
		return cientificos;
	}

	public void setCientificos(Cientificos cientificos) {
		this.cientificos = cientificos;
	}

	public Proyecto getProyecto() {
		return proyecto;
	}

	public void setProyecto(Proyecto proyecto) {
		this.proyecto = proyecto;
	}

	@Override
	public String toString() {
		return "Asignado_a [id=" + id + ", cientificos=" + cientificos + ", proyecto=" + proyecto + "]";
	}
	
	
    
    
}
