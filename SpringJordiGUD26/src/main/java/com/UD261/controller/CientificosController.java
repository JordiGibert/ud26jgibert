package com.UD261.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.UD261.dto.Cientificos;
import com.UD261.service.CientificosServiceImpl;



@RestController
@RequestMapping("/api")
public class CientificosController {
	
	@Autowired
	CientificosServiceImpl cientificosServiceImpl;
	
	@GetMapping("/cientificos")
	public List<Cientificos> listarCcursos(){
		return cientificosServiceImpl.listarCientificos();
	}
	
	
	@PostMapping("/cientificos")
	public Cientificos salvarCientificos(@RequestBody Cientificos cientificos) {
		
		return cientificosServiceImpl.guardarCientificos(cientificos);
	}
	
	
	@GetMapping("/cientificos/{DNI}")
	public Cientificos CientificosXID(@PathVariable(name="DNI") String DNI) {
		
		Cientificos Cientificos_xid= new Cientificos();
		
		Cientificos_xid=cientificosServiceImpl.cientificosXID(DNI);
		
		System.out.println("Cientificos XID: "+Cientificos_xid);
		
		return Cientificos_xid;
	}
	
	@PutMapping("/cientificos/{DNI}")
	public Cientificos actualizarCientificos(@PathVariable(name="DNI")String DNI,@RequestBody Cientificos Cientificos) {
		
		Cientificos Cientificos_seleccionado= new Cientificos();
		Cientificos Cientificos_actualizado= new Cientificos();
		
		Cientificos_seleccionado= cientificosServiceImpl.cientificosXID(DNI);
		
		Cientificos_seleccionado.setNom_apels(Cientificos.getNom_apels());
		
		Cientificos_actualizado = cientificosServiceImpl.actualizarCientificos(Cientificos_seleccionado);
		
		System.out.println("El Cientifico actualizado es: "+ Cientificos_actualizado);
		
		return Cientificos_actualizado;
	}
	
	@DeleteMapping("/cientificos/{DNI}")
	public void eleiminarCientificos(@PathVariable(name="DNI")String DNI) {
		cientificosServiceImpl.eliminarCientificos(DNI);
	}

}
