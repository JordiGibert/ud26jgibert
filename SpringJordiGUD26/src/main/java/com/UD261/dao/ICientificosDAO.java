package com.UD261.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.UD261.dto.Cientificos;

public interface ICientificosDAO extends JpaRepository<Cientificos, String> {

}
