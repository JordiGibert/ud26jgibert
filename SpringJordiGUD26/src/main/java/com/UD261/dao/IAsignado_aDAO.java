package com.UD261.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.UD261.dto.Asignado_a;

public interface IAsignado_aDAO extends JpaRepository<Asignado_a, Integer>{

}
