package com.UD261.dao;

import org.springframework.data.jpa.repository.JpaRepository;


import com.UD261.dto.Proyecto;

public interface IProyectoDAO extends JpaRepository<Proyecto, String>{

}
