package com.UD261;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringJordiGud26Application {

	public static void main(String[] args) {
		SpringApplication.run(SpringJordiGud26Application.class, args);
	}

}
