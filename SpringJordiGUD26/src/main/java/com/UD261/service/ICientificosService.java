package com.UD261.service;

import java.util.List;

import com.UD261.dto.Cientificos;



public interface ICientificosService {
	public List<Cientificos> listarCientificos(); //Listar All 
	
	public Cientificos guardarCientificos(Cientificos cientificos);	//Guarda un Curso CREATE
	
	public Cientificos cientificosXID(String DNI); //Leer datos de un Curso READ
	
	public Cientificos actualizarCientificos(Cientificos cientificos); //Actualiza datos del Curso UPDATE
	
	public void eliminarCientificos(String DNI);// Elimina el Curso DELETE
}
