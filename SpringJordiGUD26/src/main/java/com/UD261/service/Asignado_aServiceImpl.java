package com.UD261.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.UD261.dao.IAsignado_aDAO;
import com.UD261.dto.Asignado_a;

@Service
public class Asignado_aServiceImpl implements IAsignado_aService {

	@Autowired
	IAsignado_aDAO iAsignado_aDAO;
	
	@Override
	public List<Asignado_a> listarAsignado_a() {
		// TODO Auto-generated method stub
		return iAsignado_aDAO.findAll();
	}

	@Override
	public Asignado_a guardarAsignado_a(Asignado_a asignado_a) {
		// TODO Auto-generated method stub
		return iAsignado_aDAO.save(asignado_a);
	}

	@Override
	public Asignado_a asignado_aXID(int id) {
		// TODO Auto-generated method stub
		return iAsignado_aDAO.findById(id).get();
	}

	@Override
	public Asignado_a actualizarAsignado_a(Asignado_a asignado_a) {
		// TODO Auto-generated method stub
		return iAsignado_aDAO.save(asignado_a);
	}

	@Override
	public void eliminarAsignado_a(int id) {
		// TODO Auto-generated method stub
		iAsignado_aDAO.deleteById(id);
		
	}

}
