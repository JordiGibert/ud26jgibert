package com.UD261.service;

import java.util.List;

import com.UD261.dto.Asignado_a;



public interface IAsignado_aService {
	public List<Asignado_a> listarAsignado_a(); //Listar All 
	
	public Asignado_a guardarAsignado_a(Asignado_a asignado_a);	//Guarda un RegistroCurso CREATE
	
	public Asignado_a asignado_aXID(int id); //Leer datos de un RegistroCurso READ
	
	public Asignado_a actualizarAsignado_a(Asignado_a asignado_a); //Actualiza datos del RegistroCurso UPDATE
	
	public void eliminarAsignado_a(int id);// Elimina el RegistroCurso DELETE
}
