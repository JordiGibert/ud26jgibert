package com.UD261.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.UD261.dao.ICientificosDAO;
import com.UD261.dto.Cientificos;

@Service
public class CientificosServiceImpl implements ICientificosService{

	
	@Autowired
	ICientificosDAO iCientificosDAO;
	
	@Override
	public List<Cientificos> listarCientificos() {
		// TODO Auto-generated method stub
		return iCientificosDAO.findAll();
	}

	@Override
	public Cientificos guardarCientificos(Cientificos cientificos) {
		// TODO Auto-generated method stub
		return iCientificosDAO.save(cientificos);
	}

	@Override
	public Cientificos cientificosXID(String DNI) {
		// TODO Auto-generated method stub
		return iCientificosDAO.findById(DNI).get();
	}

	@Override
	public Cientificos actualizarCientificos(Cientificos cientificos) {
		// TODO Auto-generated method stub
		return iCientificosDAO.save(cientificos);
	}

	@Override
	public void eliminarCientificos(String DNI) {
		// TODO Auto-generated method stub
		iCientificosDAO.deleteById(DNI);
	}

}
